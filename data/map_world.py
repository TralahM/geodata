#!/usr/bin/env python
import json
import matplotlib.pyplot as plt

world = json.load(open("world.json", 'r', encoding="utf-8-sig"))


def map_country(index):
    features = world['features']
    country_name = features[index]['properties']['NAME'].replace(" ", "_")
    fig = plt.figure(figsize=(20, 16))
    ax = fig.add_subplot(111)
    print(index+1, " ", country_name)
    # Get coordinate data
    try:
        coords = features[index]['geometry']['coordinates']
        if len(coords) is 1:
            X, Y = [i[0] for i in coords[0]], [i[1] for i in coords[0]]
            ax.plot(X, Y)
        else:
            for k in range(len(coords)):
                X, Y = [i[0] for i in coords[k][0]], [i[1]
                                                      for i in coords[k][0]]
                ax.plot(X, Y)
    except:
        pass
    plt.savefig("maps/"+str(index)+"_"+country_name+".png")
    plt.close()
    del ax
    del fig


if __name__ == '__main__':
    for i in range(len(world['features'])):
        map_country(i)
