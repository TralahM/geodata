#!/usr/bin/env python
import json
import matplotlib.pyplot as plt

world=json.load(open("world.json",'r',encoding="utf-8-sig"))

def map_country(index):
    features=world['features']
    country_name=features[index]['properties']['NAME'].replace(" ","_")
    fig=plt.figure(figsize=(20,16))
    ax=fig.add_subplot(111)
    print(index+1," ",country_name)
    # Get coordinate data
    try:
        coords=features[index]['geometry']['coordinates'][1][0]
        X,Y=[i[0] for i in coords],[i[1] for i in coords]
    except :
        coords=features[index]['geometry']['coordinates'][0]
        X,Y=[i[0] for i in coords],[i[1] for i in coords]
    ax.plot(X,Y)
    plt.savefig("maps/"+str(index)+"_"+country_name+".png")
if __name__=='__main__':
    for i in range(len(world['features'])):
        map_country(i)