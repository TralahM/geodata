#!/usr/bin/env python
from argparse import ArgumentParser
import pandas as pd

df = pd.read_csv("colonial_data.csv")
# print(df.shape)
# df.to_csv("colonial_data.csv")


def n_colonies_by(power='UK'):
    d = df[df['Principal Colonial Power'] == power]
    print("{0} had {1} colonies..".format(power, d.shape[0]))
    print(d)


if __name__ == '__main__':
    parser = ArgumentParser(
        epilog='Author: Tralah M Brian <musyoki.tralah@students.jkuat.ac.ke>')
    parser.add_argument('-c', '--colonial_power', action='store', dest='power',
                        help='The colonial power to get info on.', default='France', choices=list(set(df['Principal Colonial Power'])))
    args = parser.parse_args()
    n_colonies_by(args.power)
